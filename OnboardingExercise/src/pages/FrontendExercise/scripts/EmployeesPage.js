import { ref } from "vue";
import axios from "axios";

export default {
  data() {
    return {
      search: "",
      date: null,
      tableData: [],
      columns: [
        {
          name: "id",
          required: true,
          label: "Employee ID",
          align: "left",
          field: (row) => row.id,
          sortable: true,
        },
        {
          name: "name",
          align: "left",
          label: "Name",
          field: "name",
          sortable: true,
        },
        {
          name: "email",
          align: "left",
          label: "Email",
          field: "email",
          sortable: true,
        },
        {
          name: "status",
          align: "left",
          label: "Status",
          field: "status",
          sortable: true,
        },
        {
          name: "address",
          align: "left",
          label: "Address",
          field: "address",
          sortable: true,
        },
        {
          name: "actions",
          align: "left",
          field: "actions",
          sortable: false,
          headerStyle: "width: 24px;",
        },
      ],
      pagination: {
        rowsPerPage: 10,
        page: 1,
        sortBy: "desc",
        descending: false,
      },
      model: ref(null),
      options: ["All", "Employee ID", "Name", "Email", "Status", "Address"],
    };
  },
  computed: {
    filteredData() {
      let data = this.tableData;

      // Filter based on the search term if applicable
      if (this.search) {
        const searchTerm = this.search.toLowerCase();
        data = data.filter((row) =>
          Object.values(row).some((value) =>
            value.toString().toLowerCase().includes(searchTerm)
          )
        );
      }

      return data;
    },
    displayedColumns() {
      if (this.model && this.model !== "All") {
        return this.columns.filter((column) => column.label === this.model);
      }
      return this.columns;
    },
  },
  mounted() {
    this.fetchData();
  },
  methods: {
    fetchData() {
      axios
        .get("http://localhost:3000/employees")
        .then((response) => {
          this.tableData = response.data;
          // Ensure the page is set correctly after data fetch
          this.pagination.page = 1;
        })
        .catch((error) => {
          console.error("There was an error fetching the data", error);
        });
    },
  },
  mounted() {
    this.fetchData();
  },
};
