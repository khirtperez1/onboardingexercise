import { ref, defineComponent } from "vue";
import axios from "axios";
import { useRouter } from "vue-router";

export default defineComponent({
  name: "EmployeesForm",
  setup() {
    const router = useRouter();
    const name = ref("");
    const id = ref("");
    const email = ref("");
    const task = ref("");
    const description = ref("");
    const status = ref("");
    const address = ref("");

    const submitForm = async () => {
      console.log("test");
      const employees = {
        name: name.value,
        id: id.value,
        email: email.value,
        task: task.value,
        description: description.value,
        status: status.value,
        address: address.value,
      };

      console.log("test:", employees);
      try {
        await saveUserToServer(employees);
        navigateToTable();
        clearForm();
      } catch (error) {
        console.error("Error saving user:", error);
      }
    };

    const saveUserToServer = async (employees) => {
      await axios.post(`http://localhost:3000/employees`, employees);
      console.log("Added");
    };

    const clearForm = () => {
      name.value = "";
      email.value = "";
      id.value = "";
      task.value = "";
      description.value = "";
      status.value = "";
      address.value = "";
    };

    const navigateToTable = () => {
      router.push("/employees");
    };

    return {
      name,
      id,
      email,
      task,
      description,
      navigateToTable,
      status,
      address,
      submitForm,
    };
  },
});
