const routes = [
  {
    path: "/",
    redirect: {
      name: "dashboard",
    },
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: () => import("pages/FrontendExercise/DashboardPage.vue"),
      },
      {
        path: "employees",
        name: "employees",
        component: () => import("pages/FrontendExercise/EmployeesPage.vue"),
      },
      {
        path: "forms",
        name: "forms",
        component: () => import("pages/FrontendExercise/EmployeesForm.vue"),
      },
    ],
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
